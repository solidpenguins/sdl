/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: lb
 *
 * Created on 8 maggio 2016, 16.43
 */

#include <cstdlib>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <string>

const int sWidth = 1024;
const int sHeight = 768;

//using namespace std;

enum KeyEvent{
    KEY_PRESS_SURFACE_DEFAULT,
    KEY_PRESS_SURFACE_UP,
    KEY_PRESS_SURFACE_DOWN,
    KEY_PRESS_SURFACE_LEFT,
    KEY_PRESS_SURFACE_RIGHT,
    KEY_PRESS_SURFACE_TOTAL
};


bool init();
bool loadMedia();
void close();

SDL_Surface* loadSurface(std::string path);

SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Surface* gKeyPressSurfaces[KEY_PRESS_SURFACE_TOTAL];
SDL_Surface* gCurrentSurface = NULL;

SDL_Surface* gStretchedSurface = NULL;


bool init(){
    bool success = true;
    if(SDL_Init(SDL_INIT_VIDEO)<0){
        printf("SDL ERROR: %s/n",SDL_GetError());
        success = false;
    } else {
        gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sWidth, sHeight, SDL_WINDOW_SHOWN );
        if(gWindow == NULL){
            printf("SDL WINDOW ERROR: %s/n",SDL_GetError());
            success = false;
        } else {
            gScreenSurface = SDL_GetWindowSurface(gWindow);
        }
    }
    return success;
}

bool loadMedia(){
    bool success = true;
    
    
    gStretchedSurface = loadSurface("stretch.bmp");
    if(gStretchedSurface == NULL){
        printf("Failed");
        success = false;
    }
    
    /*
    gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadSurface("press.bmp");
    if(gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL){
        printf("SDL LOAD IMAGE ERROR: %s/n",SDL_GetError());
        success = false;
    }

    gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] = loadSurface("up.bmp");
    if(gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] == NULL){
        printf("SDL LOAD IMAGE ERROR: %s/n",SDL_GetError());
        success = false;
    }    
    
    gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] = loadSurface("down.bmp");
    if(gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] == NULL){
        printf("SDL LOAD IMAGE ERROR: %s/n",SDL_GetError());
        success = false;
    }

    gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] = loadSurface("left.bmp");
    if(gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] == NULL){
        printf("SDL LOAD IMAGE ERROR: %s/n",SDL_GetError());
        success = false;
    }    

    gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] = loadSurface("right.bmp");
    if(gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] == NULL){
        printf("SDL LOAD IMAGE ERROR: %s/n",SDL_GetError());
        success = false;
    }   
    */
    return success;
}

void close(){
    /*
    for( int i = 0; i < KEY_PRESS_SURFACE_TOTAL; ++i ){
        SDL_FreeSurface( gKeyPressSurfaces[ i ] );
        gKeyPressSurfaces[ i ] = NULL;
    } 
     */
      
    SDL_FreeSurface(gStretchedSurface);
    gStretchedSurface = NULL;
    
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;
    SDL_Quit();
}

SDL_Surface* loadSurface(std::string path){
    SDL_Surface* optimizedSurface = NULL;
    SDL_Surface* loadedSurface = SDL_LoadBMP(path.c_str());
    if(loadedSurface == NULL){
        printf( "Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
    } else {
        optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format,NULL);
        if(optimizedSurface == NULL){
            printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
        } 
       SDL_FreeSurface(loadedSurface); 
    }
   
    return optimizedSurface;
}



int main(int argc, char* args[]) {
    
    if(!init()){
        printf("FAIL TO START");
    } else {
        if(!loadMedia()){
            printf("FAIL TO LOAD IMAGE");
        } else {
            bool quit = false;
            SDL_Event e;
            gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
            while(!quit){
                while(SDL_PollEvent(&e)!=0){
                    if(e.type == SDL_QUIT){
                        quit = true;
                    } 
                    /*
                    else if(e.type == SDL_KEYDOWN){                
                        switch(e.key.keysym.sym){
                            case SDLK_UP:
                                gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ];
                            break;
                            case SDLK_DOWN:
                                gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ];
                            break;
                            case SDLK_LEFT:
                                gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ];
                            break;
                            case SDLK_RIGHT:
                                gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ];
                            break;
                        }
                    } */
                    
                }
                
                SDL_Rect stretchRect;
                stretchRect.x = 0;
                stretchRect.y = 0;
                stretchRect.w = sWidth;
                stretchRect.h = sHeight;

                SDL_BlitScaled( gStretchedSurface, NULL, gScreenSurface, &stretchRect );
                
                //SDL_BlitSurface(gCurrentSurface,NULL,gScreenSurface,NULL);
                SDL_UpdateWindowSurface(gWindow);
            }
        }  
    }
    
    close();
    return 0;
}

